package com.hy.consumer.sentinel;

import com.hy.consumer.service.ProductService;
import org.springframework.stereotype.Component;

@Component
public class ProductFallback implements ProductService {

    @Override
    public String getHello() {
        return "interface break down !";
    }
}
