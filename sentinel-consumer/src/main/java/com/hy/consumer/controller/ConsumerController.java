package com.hy.consumer.controller;

import com.hy.consumer.service.ProductService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class ConsumerController {

    @Resource
    private ProductService productService;

    @GetMapping("/sayHello")
    public String getHello(){
        return productService.getHello();
    }


}
