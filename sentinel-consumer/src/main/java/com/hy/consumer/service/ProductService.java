package com.hy.consumer.service;


import com.hy.consumer.sentinel.ProductFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient(value = "sentinel-product",fallback = ProductFallback.class)
public interface ProductService {

    @GetMapping("/sayHello")
    String getHello();

}
