package com.hy.product.controller;

import com.hy.product.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SayController {
    @Autowired
    ProductService productService;


    @GetMapping("/sayHello")
    public String getHello(){
        return productService.getHello();
    }


}
