package com.hy.product.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SentinelControllerAnnoation {
    /**
     * Sentinel支持通过@SentinelResource定义资源
     * 并配置blockHandler函数来进行限流之后的处理。
     * @return
     */
    @RequestMapping("/an")
    @SentinelResource(value = "Sentinel_Ann",blockHandler = "blockHandler")
    public String an(){
        return "hello sentinel";
    }


    public String blockHandler(BlockException blockException){
        blockException.printStackTrace();
        return "系统繁忙请稍后再试";
    }
}
