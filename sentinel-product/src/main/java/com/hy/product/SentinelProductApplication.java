package com.hy.product;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SentinelProductApplication {

    public static void main(String[] args) {
        SpringApplication.run(SentinelProductApplication.class, args);
    }

}
